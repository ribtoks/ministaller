module github.com/ribtoks/ministaller

go 1.19

require (
	github.com/ribtoks/gform v0.0.0-20171008190928-632039b45d47 // indirect
	github.com/ribtoks/w32 v0.0.0-20161110083638-059610a2aaa4 // indirect
	gitlab.com/ribtoks-vendors/gform v0.0.0-20171008190928-632039b45d47
	gitlab.com/ribtoks-vendors/w32 v0.0.0-20161110083638-059610a2aaa4
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)
